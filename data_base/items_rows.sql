INSERT OR IGNORE INTO items (name, price)
VALUES ('Beer Can', 1.2),
       ('Beer Pack (x6)', 5.0),
       ('Guitar', 250.60),
       ('Piano', 600.5),
       ('An Item, you definitely want to get this one', 20.0),
       ('Design Patterns Assignment', 55.0),
       ('Strange Ace Ventura Costume', 68.9),
       ('Right, This is enough', 3.0);
