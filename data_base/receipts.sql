CREATE TABLE IF NOT EXISTS receipts (
    receipt_id  TEXT    PRIMARY KEY,
    total_price FLOAT   NOT NULL,
    date        TEXT    NOT NULL,
    is_open     BOOLEAN NOT NULL
);

CREATE TABLE IF NOT EXISTS receipt_entries (
    receipt_id  TEXT,
    item        TEXT,
    unit_price  FLOAT   NOT NULL,
    units       INTEGER NOT NULL,
    total_price FLOAT   NOT NULL,

    UNIQUE (receipt_id, item),
    FOREIGN KEY (receipt_id)    REFERENCES receipts(receipt_id),
    FOREIGN KEY (item)          REFERENCES items(name)
);
