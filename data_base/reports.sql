CREATE TABLE IF NOT EXISTS reports (
    date            TEXT    PRIMARY KEY,
    total_revenue   FLOAT   NOT NULL,
    closed_receipts INTEGER NOT NULL
);

CREATE TABLE IF NOT EXISTS report_entries (
    date    TEXT,
    item    TEXT,
    units   INTEGER NOT NULL,
    revenue FLOAT   NOT NULL,

    UNIQUE (date, item),
    FOREIGN KEY (date) REFERENCES reports(date),
    FOREIGN KEY (item) REFERENCES items(name)
);
