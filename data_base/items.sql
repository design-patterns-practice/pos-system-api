CREATE TABLE IF NOT EXISTS items (
    name    TEXT    PRIMARY KEY,
    price   FLOAT   NOT NULL
);
