import os
from typing import Callable, Final, Optional, TypeVar

from core import (
    IItemRepository,
    IReceiptRepository,
    IReportRepository,
    POSSystemService,
    ReceiptInteractor,
    ReportInteractor,
)
from fastapi import FastAPI
from infra import (
    AutoCloseable,
    SQLiteConnectionFactory,
    SQLiteItemRepository,
    SQLiteReceiptRepository,
    SQLiteReportRepository,
    help_api,
    receipt_api,
    report_api,
)

ITEMS_SQL: Final[str] = "data_base/items.sql"
ITEMS_ROWS_SQL: Final[str] = "data_base/items_rows.sql"
RECEIPTS_SQL: Final[str] = "data_base/receipts.sql"
REPORTS_SQL: Final[str] = "data_base/reports.sql"
POS_STORE_DB: Final[str] = "data_base/pos_store.sqlite"

T = TypeVar("T", bound=AutoCloseable)
S = TypeVar("S")


def with_shutdown_hook(function: Callable[[FastAPI], T]) -> Callable[[FastAPI], T]:
    """Adds a shutdown hook for the resource returned by the specified callable."""

    def wrapper(app: FastAPI) -> T:
        repository = function(app)

        @app.on_event("shutdown")
        def shutdown_hook() -> None:
            print(f"Shutting down resource. Class: {repository.__class__.__name__}")
            repository.close()

        return repository

    return wrapper


def with_db_file(function: Callable[[FastAPI], S]) -> Callable[[FastAPI], S]:
    """Will create a default DB file for SQLite if one doesn't exist."""

    def wrapper(app: FastAPI) -> S:
        if not os.path.isfile(POS_STORE_DB):
            open(POS_STORE_DB, "w").close()

        return function(app)

    return wrapper


@with_db_file
@with_shutdown_hook
def get_sqlite_item_repository(_app: FastAPI) -> SQLiteItemRepository:
    """Returns a preconfigured SQLite item repository."""
    return SQLiteItemRepository(
        connection_factory=SQLiteConnectionFactory(db_file=POS_STORE_DB),
        init_files=[ITEMS_SQL, ITEMS_ROWS_SQL],
    )


@with_db_file
@with_shutdown_hook
def get_sqlite_receipt_repository(_app: FastAPI) -> SQLiteReceiptRepository:
    """Returns a preconfigured SQLite receipt repository."""
    return SQLiteReceiptRepository(
        connection_factory=SQLiteConnectionFactory(db_file=POS_STORE_DB),
        init_files=[RECEIPTS_SQL],
    )


@with_db_file
@with_shutdown_hook
def get_sqlite_report_repository(_app: FastAPI) -> SQLiteReportRepository:
    """Returns a preconfigured SQLite report repository."""
    return SQLiteReportRepository(
        connection_factory=SQLiteConnectionFactory(db_file=POS_STORE_DB),
        init_files=[REPORTS_SQL],
    )


def setup(
    item_repository: Optional[IItemRepository] = None,
    receipt_repository: Optional[IReceiptRepository] = None,
    report_repository: Optional[IReportRepository] = None,
) -> FastAPI:
    app = FastAPI()
    app.include_router(receipt_api)
    app.include_router(report_api)
    app.include_router(help_api)

    item_repository = item_repository or get_sqlite_item_repository(app)
    receipt_repository = receipt_repository or get_sqlite_receipt_repository(app)
    report_repository = report_repository or get_sqlite_report_repository(app)

    app.state.core = POSSystemService(
        receipt_interactor=ReceiptInteractor(
            item_repository=item_repository, receipt_repository=receipt_repository
        ),
        report_interactor=ReportInteractor(
            receipt_repository=receipt_repository, report_repository=report_repository
        ),
    )

    return app
