from dataclasses import dataclass
from typing import Dict, Sequence

ItemName = str


class InMemoryItemRepository:
    @dataclass(frozen=True)
    class ItemEntry:
        name: ItemName
        price: float

    def __init__(self) -> None:
        """Creates a blank in memory item repository."""
        self.__items: Dict[ItemName, InMemoryItemRepository.ItemEntry] = {}

    def get_item_price(self, item_name: str) -> float:
        """Returns the price of the item with the specified name."""
        assert item_name in self.__items, f"The item {item_name} does not exist."

        return self.__items[item_name].price

    def has_item(self, item_name: str) -> bool:
        """Returns true if the item with the specified name exists, false otherwise"""
        return item_name in self.__items

    def add_items(self, entries: Sequence[ItemEntry]) -> None:
        """Adds the specified item entries to the database."""
        for entry in entries:
            self.add_item(entry)

    def add_item(self, entry: ItemEntry) -> None:
        """Adds a single item entry to the database."""
        assert entry.name not in self.__items, f"Item {entry.name} is already present."

        self.__items[entry.name] = entry
