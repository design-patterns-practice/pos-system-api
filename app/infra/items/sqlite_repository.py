from dataclasses import dataclass
from typing import Sequence

from infra.data_bases import SQLiteRepository


class SQLiteItemRepository(SQLiteRepository):
    @dataclass
    class ItemEntry:
        name: str
        price: float

    def get_item_price(self, item_name: str) -> float:
        """Returns the price of the item with the specified name."""
        assert self.has_item(
            item_name
        ), f"The item with the name {item_name} does not exist."

        result = self.query(
            "SELECT price FROM items WHERE name=:name", {"name": item_name}
        )
        return float(result.fetchone()[0])

    def has_item(self, item_name: str) -> bool:
        """Returns true if the item with the specified name exists, false otherwise"""
        result = self.query(
            "SELECT COUNT(*) FROM items WHERE name=:name", {"name": item_name}
        )
        return float(result.fetchone()[0]) > 0

    def add_items(self, entries: Sequence[ItemEntry]) -> None:
        """Adds the specified item entries to the database."""
        for entry in entries:
            self.add_item(entry)

    def add_item(self, entry: ItemEntry) -> None:
        """Adds a single item entry to the database."""
        assert not self.has_item(entry.name), f"Item {entry.name} is already present."

        self.update(
            "INSERT INTO items (name, price) VALUES (:name, :price)",
            {"name": entry.name, "price": entry.price},
        )
