from datetime import date
from typing import Dict, List, Sequence
from uuid import uuid4

from core import Receipt, ReceiptStatus


class InMemoryReceiptRepository:
    def __init__(self) -> None:
        """Creates a blank in memory receipt repository."""
        self.__receipts: Dict[str, Receipt] = {}

    def get_receipts(self, *, receipt_date: date) -> List[Receipt]:
        """Returns all receipts with the given date."""
        return [
            receipt
            for receipt in self.__receipts.values()
            if receipt.receipt_date == receipt_date
        ]

    def get_receipt(self, receipt_id: str) -> Receipt:
        """Returns the receipt with the specified ID."""
        assert receipt_id in self.__receipts

        return self.__receipts[receipt_id]

    def create_receipt(self, *, receipt_date: date) -> str:
        """Creates a new blank receipt and returns it's ID."""
        receipt_id = str(uuid4())
        assert (
            receipt_id not in self.__receipts
        ), f"The receipt with the ID {receipt_id} already exists."

        self.__receipts[receipt_id] = Receipt(
            id=receipt_id,
            entries=[],
            total_price=0.0,
            status=ReceiptStatus.OPEN,
            receipt_date=receipt_date,
        )
        return receipt_id

    def update_receipt(self, *, receipt_id: str, receipt: Receipt) -> None:
        """Updates the receipt with the specified ID with the given receipt data."""
        assert (
            receipt_id in self.__receipts
        ), f"The receipt with the ID {receipt_id} does not exist."
        assert (
            receipt_id == receipt.id
        ), f"The specified ID {receipt_id} is different from the one in the receipt {receipt.id}."

        self.__receipts[receipt_id] = receipt

    def has_receipt(self, receipt_id: str) -> bool:
        """Returns true if a receipt with the specified ID is present, false otherwise."""
        return receipt_id in self.__receipts

    def add_receipts(self, receipts: Sequence[Receipt]) -> None:
        """Adds the specified receipts to this in memory data base."""
        for receipt in receipts:
            self.add_receipt(receipt)

    def add_receipt(self, receipt: Receipt) -> None:
        """Adds the specified receipt to this in memory data base."""
        assert (
            receipt.id not in self.__receipts
        ), f"The receipt with the ID {receipt.id} already exists."

        self.__receipts[receipt.id] = receipt
