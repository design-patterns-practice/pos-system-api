from datetime import date
from typing import List
from uuid import uuid4

from core import Receipt, ReceiptEntry, ReceiptStatus
from infra.data_bases import SQLiteRepository


class SQLiteReceiptRepository(SQLiteRepository):
    def get_receipts(self, *, receipt_date: date) -> List[Receipt]:
        """Returns all receipts with the given date."""
        receipts_result = self.query(
            "SELECT receipt_id, total_price, is_open FROM receipts WHERE date=:receipt_date",
            {"receipt_date": receipt_date.isoformat()},
        ).fetchall()

        receipts: List[Receipt] = []
        for receipt_id, total_price, is_open in receipts_result:
            receipts.append(
                Receipt(
                    id=receipt_id,
                    entries=self.__get_receipt_entries(receipt_id),
                    total_price=total_price,
                    receipt_date=receipt_date,
                    status=ReceiptStatus.OPEN if is_open else ReceiptStatus.CLOSED,
                )
            )

        return receipts

    def get_receipt(self, receipt_id: str) -> Receipt:
        """Returns the receipt with the specified ID."""
        assert self.has_receipt(
            receipt_id
        ), f"The receipt with the ID {receipt_id} does not exist."

        total_price, receipt_date, is_open = self.query(
            "SELECT total_price, date, is_open FROM receipts WHERE receipt_id=:receipt_id",
            {"receipt_id": receipt_id},
        ).fetchone()

        return Receipt(
            id=receipt_id,
            entries=self.__get_receipt_entries(receipt_id),
            total_price=total_price,
            receipt_date=date.fromisoformat(receipt_date),
            status=ReceiptStatus.OPEN if is_open else ReceiptStatus.CLOSED,
        )

    def create_receipt(self, *, receipt_date: date) -> str:
        """Creates a new blank receipt and returns it's ID."""
        receipt_id = str(uuid4())
        assert not self.has_receipt(
            receipt_id
        ), f"The receipt with the ID {receipt_id} already exists."

        self.update(
            "INSERT INTO receipts (receipt_id, total_price, date, is_open) "
            "VALUES (:receipt_id, :total_price, :date, :is_open)",
            {
                "receipt_id": receipt_id,
                "total_price": 0.0,
                "date": receipt_date.isoformat(),
                "is_open": True,
            },
        )

        return receipt_id

    def update_receipt(self, *, receipt_id: str, receipt: Receipt) -> None:
        """Updates the receipt with the specified ID with the given receipt data."""
        assert self.has_receipt(
            receipt_id
        ), f"The receipt with the ID {receipt_id} does not exist."
        assert (
            receipt_id == receipt.id
        ), f"The specified ID {receipt_id} is different from the one in the receipt {receipt.id}."

        self.update(
            "UPDATE receipts SET total_price=:total_price, date=:receipt_date, is_open=:is_open "
            "WHERE receipt_id=:receipt_id",
            {
                "receipt_id": receipt_id,
                "total_price": receipt.total_price,
                "receipt_date": receipt.receipt_date.isoformat(),
                "is_open": receipt.status == ReceiptStatus.OPEN,
            },
        )

        self.update(
            "DELETE FROM receipt_entries WHERE receipt_id=:receipt_id",
            {"receipt_id": receipt_id},
        )

        for entry in receipt.entries:
            self.update(
                "INSERT INTO receipt_entries(receipt_id, item, unit_price, units, total_price) "
                "VALUES (:receipt_id, :item_name, :unit_price, :units, :total_price)",
                {
                    "receipt_id": receipt_id,
                    "item_name": entry.item_name,
                    "unit_price": entry.unit_price,
                    "units": entry.units,
                    "total_price": entry.total_price,
                },
            )

    def has_receipt(self, receipt_id: str) -> bool:
        """Returns true if a receipt with the specified ID is present, false otherwise."""
        receipt_count = self.query(
            "SELECT COUNT(*) FROM receipts WHERE receipt_id=:receipt_id",
            {"receipt_id": receipt_id},
        ).fetchone()[0]

        return float(receipt_count) > 0

    def __get_receipt_entries(self, receipt_id: str) -> List[ReceiptEntry]:
        """Returns all receipt entries for the receipt with the specified ID."""
        entries_result = self.query(
            "SELECT item, unit_price, units, total_price FROM receipt_entries WHERE receipt_id=:receipt_id",
            {"receipt_id": receipt_id},
        ).fetchall()

        return [
            ReceiptEntry(
                item_name=item,
                unit_price=unit_price,
                units=units,
                total_price=total_price,
            )
            for item, unit_price, units, total_price in entries_result
        ]
