from datetime import date
from typing import Dict

from core import Report


class InMemoryReportRepository:
    def __init__(self) -> None:
        """Creates a blank in memory report repository."""
        self.__reports: Dict[date, Report] = {}

    def get_report(self, *, report_date: date) -> Report:
        """Returns the report for the specified date."""
        assert (
            report_date in self.__reports
        ), f"A report for the date {report_date.isoformat()} does not exist."
        return self.__reports[report_date]

    def add_report(self, report: Report, *, report_date: date) -> None:
        """Adds the specified report for the given date."""
        assert (
            report_date not in self.__reports
        ), f"A report for the date {report_date.isoformat()} already exists."
        assert report_date == report.report_date, (
            f"The specified report date {report_date.isoformat()} is different "
            f"from the one in the report {report.report_date.isoformat()}."
        )

        self.__reports[report_date] = report

    def update_report(self, report: Report, *, report_date: date) -> None:
        """Updates the report for the specified date with the one given."""
        assert (
            report_date in self.__reports
        ), f"A report for the date {report_date.isoformat()} does not exist."
        assert report_date == report.report_date, (
            f"The specified report date {report_date.isoformat()} is different "
            f"from the one in the report {report.report_date.isoformat()}."
        )

        self.__reports[report_date] = report

    def has_report(self, *, report_date: date) -> bool:
        """Returns true if a report exists for the specified date."""
        return report_date in self.__reports
