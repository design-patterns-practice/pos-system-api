from datetime import date
from typing import List

from core import Report, ReportEntry
from infra.data_bases import SQLiteRepository


class SQLiteReportRepository(SQLiteRepository):
    def get_report(self, *, report_date: date) -> Report:
        """Returns the report for the specified date."""
        assert self.has_report(
            report_date=report_date
        ), f"A report for the date {report_date.isoformat()} does not exist."

        total_revenue, closed_receipts = self.query(
            "SELECT total_revenue, closed_receipts FROM reports "
            "WHERE date=:report_date",
            {"report_date": report_date},
        ).fetchone()

        return Report(
            entries=self.__get_report_entries(report_date=report_date),
            total_revenue=total_revenue,
            closed_receipts=closed_receipts,
            report_date=report_date,
        )

    def add_report(self, report: Report, *, report_date: date) -> None:
        """Adds the specified report for the given date."""
        assert not self.has_report(
            report_date=report_date
        ), f"A report for the date {report_date.isoformat()} already exists."
        assert report_date == report.report_date, (
            f"The specified report date {report_date.isoformat()} is different "
            f"from the one in the report {report.report_date.isoformat()}."
        )

        self.update(
            "INSERT INTO reports (date, total_revenue, closed_receipts) "
            "VALUES (:date, :total_revenue, :closed_receipts)",
            {
                "date": report_date.isoformat(),
                "total_revenue": report.total_revenue,
                "closed_receipts": report.closed_receipts,
            },
        )

        for entry in report.entries:
            self.update(
                "INSERT INTO report_entries (date, item, units, revenue) "
                "VALUES (:date, :item, :units, :revenue)",
                {
                    "date": report_date,
                    "item": entry.item,
                    "units": entry.units,
                    "revenue": entry.revenue,
                },
            )

    def update_report(self, report: Report, *, report_date: date) -> None:
        """Updates the report for the specified date with the one given."""
        assert self.has_report(
            report_date=report_date
        ), f"A report for the date {report_date.isoformat()} does not exist."
        assert report_date == report.report_date, (
            f"The specified report date {report_date.isoformat()} is different "
            f"from the one in the report {report.report_date.isoformat()}."
        )

        self.update(
            "UPDATE reports SET total_revenue=:total_revenue, closed_receipts=:closed_receipts "
            "WHERE date=:report_date",
            {
                "report_date": report_date,
                "total_revenue": report.total_revenue,
                "closed_receipts": report.closed_receipts,
            },
        )

        self.update(
            "DELETE FROM report_entries WHERE date=:report_date",
            {"report_date": report_date},
        )

        for entry in report.entries:
            self.update(
                "INSERT INTO report_entries (date, item, units, revenue) "
                "VALUES (:date, :item, :units, :revenue)",
                {
                    "date": report_date,
                    "item": entry.item,
                    "units": entry.units,
                    "revenue": entry.revenue,
                },
            )

    def has_report(self, *, report_date: date) -> bool:
        """Returns true if a report exists for the specified date."""
        report_count = self.query(
            "SELECT COUNT(*) FROM reports WHERE date=:report_date",
            {"report_date": report_date},
        ).fetchone()[0]

        return float(report_count) > 0

    def __get_report_entries(self, *, report_date: date) -> List[ReportEntry]:
        """Retrieves all report entries for the specified date."""
        entries_result = self.query(
            "SELECT item, units, revenue FROM report_entries WHERE date=:report_date",
            {"report_date": report_date},
        ).fetchall()

        return [
            ReportEntry(item=item, units=units, revenue=revenue)
            for item, units, revenue in entries_result
        ]
