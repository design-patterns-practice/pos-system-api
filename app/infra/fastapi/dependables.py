from core import POSSystemService
from starlette.requests import Request


def get_pos_system_service(request: Request) -> POSSystemService:
    return request.app.state.core  # type: ignore
