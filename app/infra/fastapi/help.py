from fastapi import APIRouter
from infra.fastapi.responses import FetchHelpResponse, Wrapped, wrap

help_api = APIRouter()


@help_api.get("/", response_model=Wrapped[FetchHelpResponse])
def fetch_help() -> Wrapped[FetchHelpResponse]:
    return wrap(
        call=lambda: FetchHelpResponse(
            "Hello, this is a simple POS System created using FastAPI."
        )
    )
