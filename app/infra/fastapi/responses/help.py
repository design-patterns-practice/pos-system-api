from dataclasses import dataclass


@dataclass
class FetchHelpResponse:
    message: str
