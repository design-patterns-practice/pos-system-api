from typing import Callable, Dict, Generic, Optional, Type, TypeVar

from core import Error
from infra.fastapi.responses.status import ResponseStatus
from pydantic.generics import GenericModel
from starlette.responses import Response

ResponseT = TypeVar("ResponseT")


class Wrapped(GenericModel, Generic[ResponseT]):
    """This is a generic model for all responses. Positive responses will have
    the response field set, and negative responses will have the error message."""

    status: ResponseStatus
    response: Optional[ResponseT]
    error_message: Optional[str]


T = TypeVar("T")
HTTPStatusCode = int


def wrap(
    *,
    call: Callable[[], T],
    response: Optional[Response] = None,
    expected_errors: Optional[Dict[Type[Error], HTTPStatusCode]] = None,
) -> Wrapped[T]:
    """Invokes the specified callable to retrieve a response and wraps it accordingly.

    If the call is successful, the response will include whatever was returned from the call.
    If an expected error is raised, the appropriate status code and error message will be included in the response.

    Any unexpected error will be reraised."""
    if expected_errors is None:
        expected_errors = {}

    try:
        return Wrapped(
            status=ResponseStatus.SUCCESS,
            response=call(),
        )
    except Error as e:
        if type(e) not in expected_errors:
            raise e

        if response is not None:
            response.status_code = expected_errors[type(e)]

        return Wrapped(status=ResponseStatus.ERROR, error_message=e.message)
