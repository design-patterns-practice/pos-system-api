from enum import Enum


class ResponseStatus(str, Enum):
    ERROR = "ERROR"
    SUCCESS = "SUCCESS"
