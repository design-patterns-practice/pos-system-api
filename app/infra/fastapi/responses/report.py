from typing import List

from core import ReportEntry
from core.reports import Report
from pydantic import BaseModel


class ReportEntryModel(BaseModel):
    item: str
    units: int
    revenue: float

    @staticmethod
    def get_from(entry: ReportEntry) -> "ReportEntryModel":
        return ReportEntryModel(
            item=entry.item,
            units=entry.units,
            revenue=entry.revenue,
        )


class ReportModel(BaseModel):
    entries: List[ReportEntryModel]
    total_revenue: float
    closed_receipts: int
    report_date: str

    @staticmethod
    def get_from(report: Report) -> "ReportModel":
        return ReportModel(
            entries=[ReportEntryModel.get_from(entry) for entry in report.entries],
            total_revenue=report.total_revenue,
            closed_receipts=report.closed_receipts,
            report_date=report.report_date.isoformat(),
        )


class FetchReportResponse(BaseModel):
    report: ReportModel


class CreateReportResponse(BaseModel):
    report: ReportModel
