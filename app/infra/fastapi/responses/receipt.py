from typing import List

from core.receipts.receipt import Receipt, ReceiptEntry, ReceiptStatus
from pydantic import BaseModel


class ReceiptEntryModel(BaseModel):
    item_name: str
    unit_price: float
    units: int
    total_price: float

    @staticmethod
    def get_from(entry: ReceiptEntry) -> "ReceiptEntryModel":
        return ReceiptEntryModel(
            item_name=entry.item_name,
            unit_price=entry.unit_price,
            units=entry.units,
            total_price=entry.total_price,
        )


class ReceiptModel(BaseModel):
    id: str
    entries: List[ReceiptEntryModel]
    total_price: float
    receipt_date: str
    status: ReceiptStatus

    @staticmethod
    def get_from(receipt: Receipt) -> "ReceiptModel":
        return ReceiptModel(
            id=receipt.id,
            entries=[ReceiptEntryModel.get_from(entry) for entry in receipt.entries],
            total_price=receipt.total_price,
            receipt_date=receipt.receipt_date.isoformat(),
            status=receipt.status,
        )


class FetchReceiptResponse(BaseModel):
    receipt: ReceiptModel  # The retrieved receipt.


class CreateReceiptResponse(BaseModel):
    receipt_id: str  # The ID of the created receipt.


class UpdateReceiptResponse(BaseModel):
    receipt: ReceiptModel  # The updated receipt.


class CloseReceiptResponse(BaseModel):
    receipt: ReceiptModel  # The closed receipt.
