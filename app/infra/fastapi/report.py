from datetime import date

from core import InvalidReportDateError, POSSystemService
from fastapi import APIRouter, Depends
from infra.fastapi.dependables import get_pos_system_service
from infra.fastapi.responses import (
    CreateReportResponse,
    FetchReportResponse,
    ReportModel,
    Wrapped,
    wrap,
)
from starlette import status
from starlette.responses import Response

report_api = APIRouter()


@report_api.get(
    path="/reports/{report_date}",
    response_model=Wrapped[FetchReportResponse],
    status_code=status.HTTP_200_OK,
)
def fetch_report(
    report_date: str,
    response: Response,
    core: POSSystemService = Depends(get_pos_system_service),
) -> Wrapped[FetchReportResponse]:
    """Returns the latest daily report for the specified date.

    Will return an error response if:
     - A Report for the specified date does not exist.

    Note:
     - Only closed receipts are included in the report."""
    return wrap(
        call=lambda: FetchReportResponse(
            report=ReportModel.get_from(
                core.fetch_report(report_date=date.fromisoformat(report_date))
            )
        ),
        response=response,
        expected_errors={InvalidReportDateError: status.HTTP_404_NOT_FOUND},
    )


@report_api.put(
    path="/reports/{report_date}",
    response_model=Wrapped[CreateReportResponse],
    status_code=status.HTTP_201_CREATED,
)
def create_report(
    report_date: str, core: POSSystemService = Depends(get_pos_system_service)
) -> Wrapped[CreateReportResponse]:
    """Creates a report for the specified date. If a report already exists, it will be updated.

    Note:
     - Only closed receipts are included in the report."""
    return wrap(
        call=lambda: CreateReportResponse(
            report=ReportModel.get_from(
                core.create_report(report_date=date.fromisoformat(report_date))
            )
        ),
    )
