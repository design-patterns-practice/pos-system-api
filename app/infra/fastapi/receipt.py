from typing import Dict

from core import (
    InvalidItemAmountError,
    InvalidItemError,
    InvalidReceiptIDError,
    POSSystemService,
    ReceiptAlreadyClosedError,
)
from fastapi import APIRouter, Depends
from infra.fastapi.dependables import get_pos_system_service
from infra.fastapi.responses import (
    CloseReceiptResponse,
    CreateReceiptResponse,
    FetchReceiptResponse,
    ReceiptModel,
    UpdateReceiptResponse,
    Wrapped,
    wrap,
)
from starlette import status
from starlette.responses import Response

receipt_api = APIRouter()


@receipt_api.get(
    path="/receipts/{receipt_id}",
    response_model=Wrapped[FetchReceiptResponse],
    status_code=status.HTTP_200_OK,
)
def fetch_receipt(
    receipt_id: str,
    response: Response,
    core: POSSystemService = Depends(get_pos_system_service),
) -> Wrapped[FetchReceiptResponse]:
    """Retrieves the receipt with the specified ID.

    Will return an error response if:
     - The receipt with the given ID does not exist."""
    return wrap(
        call=lambda: FetchReceiptResponse(
            receipt=ReceiptModel.get_from(core.get_receipt(receipt_id))
        ),
        response=response,
        expected_errors={InvalidReceiptIDError: status.HTTP_404_NOT_FOUND},
    )


@receipt_api.post(
    path="/receipts",
    response_model=Wrapped[CreateReceiptResponse],
    status_code=status.HTTP_201_CREATED,
)
def create_receipt(
    core: POSSystemService = Depends(get_pos_system_service),
) -> Wrapped[CreateReceiptResponse]:
    """Creates a new receipt and returns it's ID."""
    return wrap(call=lambda: CreateReceiptResponse(receipt_id=core.create_receipt()))


@receipt_api.put(
    path="/receipts/{receipt_id}/items",
    response_model=Wrapped[UpdateReceiptResponse],
    status_code=status.HTTP_200_OK,
)
def update_receipt(
    receipt_id: str,
    items: Dict[str, int],
    response: Response,
    core: POSSystemService = Depends(get_pos_system_service),
) -> Wrapped[UpdateReceiptResponse]:
    """Updates the receipt with the specified ID to contain the given items.

    Will return an error response if:
     - The receipt with the given ID does not exist.
     - The receipt is already closed.
     - One of the specified items does not exist.
     - One of the specified item amounts is invalid."""
    return wrap(
        call=lambda: UpdateReceiptResponse(
            receipt=ReceiptModel.get_from(
                core.update_receipt(receipt_id=receipt_id, items=items)
            )
        ),
        response=response,
        expected_errors={
            InvalidReceiptIDError: status.HTTP_404_NOT_FOUND,
            ReceiptAlreadyClosedError: status.HTTP_403_FORBIDDEN,
            InvalidItemError: status.HTTP_404_NOT_FOUND,
            InvalidItemAmountError: status.HTTP_422_UNPROCESSABLE_ENTITY,
        },
    )


@receipt_api.put(
    path="/receipts/{receipt_id}",
    response_model=Wrapped[CloseReceiptResponse],
    status_code=status.HTTP_200_OK,
)
def close_receipt(
    receipt_id: str,
    response: Response,
    core: POSSystemService = Depends(get_pos_system_service),
) -> Wrapped[CloseReceiptResponse]:
    """Closes the receipt with the specified ID.

    Will return an error response if:
     - The receipt with the given ID does not exist.
     - The receipt is already closed."""
    return wrap(
        call=lambda: CloseReceiptResponse(
            receipt=ReceiptModel.get_from(core.close_receipt(receipt_id))
        ),
        response=response,
        expected_errors={
            InvalidReceiptIDError: status.HTTP_404_NOT_FOUND,
            ReceiptAlreadyClosedError: status.HTTP_403_FORBIDDEN,
        },
    )
