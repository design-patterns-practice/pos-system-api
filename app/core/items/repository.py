from typing import Protocol


class IItemRepository(Protocol):
    def get_item_price(self, item_name: str) -> float:
        """Returns the price of the item with the specified name."""

    def has_item(self, item_name: str) -> bool:
        """Returns true if the item with the specified name exists, false otherwise"""
