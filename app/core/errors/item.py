from dataclasses import dataclass

from core.errors.base import Error


@dataclass(frozen=True)
class InvalidItemError(Error):
    pass
