from dataclasses import dataclass

from core.errors.base import Error


@dataclass(frozen=True)
class InvalidReceiptIDError(Error):
    pass


@dataclass(frozen=True)
class ReceiptAlreadyClosedError(Error):
    pass


@dataclass(frozen=True)
class InvalidItemAmountError(Error):
    pass
