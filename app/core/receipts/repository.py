from datetime import date
from typing import List, Protocol

from core.receipts.receipt import Receipt


class IReceiptRepository(Protocol):
    def get_receipts(self, *, receipt_date: date) -> List[Receipt]:
        """Returns all receipts with the given date."""

    def get_receipt(self, receipt_id: str) -> Receipt:
        """Returns the receipt with the specified ID."""

    def create_receipt(self, *, receipt_date: date) -> str:
        """Creates a new blank receipt and returns it's ID."""

    def update_receipt(self, *, receipt_id: str, receipt: Receipt) -> None:
        """Updates the receipt with the specified ID with the given receipt data."""

    def has_receipt(self, receipt_id: str) -> bool:
        """Returns true if a receipt with the specified ID is present, false otherwise."""
