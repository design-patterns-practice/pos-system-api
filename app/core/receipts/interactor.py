from datetime import date
from typing import Dict, List

from core import InvalidItemAmountError, InvalidItemError
from core.errors import InvalidReceiptIDError, ReceiptAlreadyClosedError
from core.items import IItemRepository
from core.receipts.receipt import Receipt, ReceiptEntry, ReceiptStatus
from core.receipts.repository import IReceiptRepository


class ReceiptInteractor:
    def __init__(
        self,
        *,
        item_repository: IItemRepository,
        receipt_repository: IReceiptRepository,
    ):
        self.__item_repository = item_repository
        self.__receipt_repository = receipt_repository

    def get_receipt(self, receipt_id: str) -> Receipt:
        """Returns the receipt with the specified receipt ID.

        :raises InvalidReceiptIDError if the receipt with the specified ID does not exist."""
        if not self.__receipt_repository.has_receipt(receipt_id):
            raise InvalidReceiptIDError(
                f"The receipt with the ID {receipt_id} does not exist."
            )

        return self.__receipt_repository.get_receipt(receipt_id)

    def create_receipt(self) -> str:
        """Creates a new blank receipt and returns it's ID."""
        return self.__receipt_repository.create_receipt(receipt_date=date.today())

    def update_receipt(self, *, receipt_id: str, items: Dict[str, int]) -> Receipt:
        """Updates the receipt with the specified ID to contain the given items.

        :returns The receipt as it is after it was updated with the items.
        :raises InvalidReceiptIDError if the receipt with the specified ID does not exist.
        :raises ReceiptAlreadyClosedError if the receipt is already closed.
        :raises InvalidItemError if an item does not exist.
        :raises InvalidItemAmountError if an item amount is not a positive number."""
        old_receipt = self.get_receipt(receipt_id)

        if old_receipt.status is ReceiptStatus.CLOSED:
            raise ReceiptAlreadyClosedError("You can't update a closed receipt.")

        new_receipt = self.__get_updated_receipt(old_receipt=old_receipt, items=items)

        self.__receipt_repository.update_receipt(
            receipt_id=receipt_id, receipt=new_receipt
        )
        return new_receipt

    def close_receipt(self, *, receipt_id: str) -> Receipt:
        """Closes the receipt with the specified ID.

        :returns The final version of the receipt after it is closed.
        :raises InvalidReceiptIDError if the receipt with the specified ID does not exist.
        :raises ReceiptAlreadyClosedError if the receipt is already closed."""
        old_receipt = self.get_receipt(receipt_id)

        if old_receipt.status is ReceiptStatus.CLOSED:
            raise ReceiptAlreadyClosedError("You can't update a closed receipt.")

        new_receipt = Receipt(
            id=old_receipt.id,
            entries=old_receipt.entries,
            total_price=old_receipt.total_price,
            status=ReceiptStatus.CLOSED,
            receipt_date=old_receipt.receipt_date,
        )

        self.__receipt_repository.update_receipt(
            receipt_id=receipt_id, receipt=new_receipt
        )

        return new_receipt

    def __get_updated_receipt(
        self, *, old_receipt: Receipt, items: Dict[str, int]
    ) -> Receipt:
        """Creates a new receipt containing all of the specified items. The ID, status and date are kept the same.

        :raises InvalidItemError if an item does not exist.
        :raises InvalidItemAmountError if an item amount is not a positive number."""
        total_price = 0.0
        receipt_entries: List[ReceiptEntry] = []

        for item, units in items.items():
            if not self.__item_repository.has_item(item):
                raise InvalidItemError(f"The item {item} does not exist.")

            if units <= 0:
                raise InvalidItemAmountError(
                    f"The amount '{units}' specified for the item '{item}' is not a positive number."
                )

            unit_price = self.__item_repository.get_item_price(item)
            total_item_price = unit_price * units

            receipt_entries.append(
                ReceiptEntry(
                    item_name=item,
                    unit_price=unit_price,
                    units=units,
                    total_price=total_item_price,
                )
            )
            total_price += total_item_price

        return Receipt(
            id=old_receipt.id,
            entries=receipt_entries,
            total_price=total_price,
            status=old_receipt.status,
            receipt_date=old_receipt.receipt_date,
        )
