from dataclasses import dataclass
from datetime import date
from enum import Enum
from typing import List


class ReceiptStatus(str, Enum):
    CLOSED = "CLOSED"
    OPEN = "OPEN"


@dataclass(frozen=True)
class ReceiptEntry:
    item_name: str
    unit_price: float
    units: int
    total_price: float


@dataclass(frozen=True)
class Receipt:
    id: str
    entries: List[ReceiptEntry]
    total_price: float
    receipt_date: date
    status: ReceiptStatus
