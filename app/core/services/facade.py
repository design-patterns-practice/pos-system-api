from datetime import date
from typing import Dict

from core import Receipt, ReceiptInteractor, Report, ReportInteractor


class POSSystemService:
    def __init__(
        self,
        *,
        receipt_interactor: ReceiptInteractor,
        report_interactor: ReportInteractor
    ):
        """Creates a service object for the POS System acting as a facade for all the underlying interactors."""
        self.__receipt_interactor = receipt_interactor
        self.__report_interactor = report_interactor

    def get_receipt(self, receipt_id: str) -> Receipt:
        """Returns the receipt with the specified ID.

        :raises InvalidReceiptIDError if the receipt with the specified ID does not exist."""
        return self.__receipt_interactor.get_receipt(receipt_id)

    def create_receipt(self) -> str:
        """Creates a new blank receipt and returns it's ID."""
        return self.__receipt_interactor.create_receipt()

    def update_receipt(self, *, receipt_id: str, items: Dict[str, int]) -> Receipt:
        """Updates the receipt with the specified ID to contain the given items.

        :returns The receipt as it is after it was updated with the items.
        :raises InvalidReceiptIDError if the receipt with the specified ID does not exist.
        :raises ReceiptAlreadyClosedError if the receipt is already closed.
        :raises InvalidItemError if an item does not exist.
        :raises InvalidItemAmountError if an item amount is not a positive number."""
        return self.__receipt_interactor.update_receipt(
            receipt_id=receipt_id, items=items
        )

    def close_receipt(self, receipt_id: str) -> Receipt:
        """Closes the receipt with the specified ID.

        :returns The final version of the receipt after it is closed.
        :raises InvalidReceiptIDError if the receipt with the specified ID does not exist.
        :raises ReceiptAlreadyClosedError if the receipt is already closed."""
        return self.__receipt_interactor.close_receipt(receipt_id=receipt_id)

    def fetch_report(self, *, report_date: date) -> Report:
        """Returns the latest daily report for the specified date.

        :raises InvalidReportDateError if a report for the specified date does not exist."""
        return self.__report_interactor.fetch_report(report_date=report_date)

    def create_report(self, *, report_date: date) -> Report:
        """Creates a report for the specified date."""
        return self.__report_interactor.create_report(report_date=report_date)
