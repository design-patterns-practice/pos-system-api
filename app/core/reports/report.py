from dataclasses import dataclass
from datetime import date
from typing import List


@dataclass(frozen=True)
class ReportEntry:
    item: str
    units: int
    revenue: float


@dataclass(frozen=True)
class Report:
    entries: List[ReportEntry]
    total_revenue: float
    closed_receipts: int
    report_date: date
