from datetime import date
from typing import Protocol

from core.reports.report import Report


class IReportRepository(Protocol):
    def get_report(self, *, report_date: date) -> Report:
        """Returns the report for the specified date."""

    def add_report(self, report: Report, *, report_date: date) -> None:
        """Adds the specified report for the given date."""

    def update_report(self, report: Report, *, report_date: date) -> None:
        """Updates the report for the specified date with the one given."""

    def has_report(self, *, report_date: date) -> bool:
        """Returns true if a report exists for the specified date."""
