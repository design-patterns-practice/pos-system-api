from datetime import date
from typing import Dict

from core import InvalidReportDateError, ReceiptStatus
from core.receipts import IReceiptRepository
from core.reports.report import Report, ReportEntry
from core.reports.repository import IReportRepository


class ReportInteractor:
    def __init__(
        self,
        *,
        receipt_repository: IReceiptRepository,
        report_repository: IReportRepository,
    ):
        self.__receipt_repository = receipt_repository
        self.__report_repository = report_repository

    def fetch_report(self, *, report_date: date) -> Report:
        """Returns the latest daily report for the specified date.

        :raises InvalidReportDateError if a report for the specified date does not exist."""
        if not self.__report_repository.has_report(report_date=report_date):
            raise InvalidReportDateError(
                f"A report for the date {report_date} does not exist."
            )

        return self.__report_repository.get_report(report_date=report_date)

    def create_report(self, *, report_date: date) -> Report:
        """Creates a report for the current day."""
        receipts = self.__receipt_repository.get_receipts(receipt_date=report_date)

        # Note: This filtering would be faster on the repository side.
        receipts = [
            receipt for receipt in receipts if receipt.status == ReceiptStatus.CLOSED
        ]

        report_entries: Dict[str, ReportEntry] = {}
        total_revenue = sum([receipt.total_price for receipt in receipts])
        closed_receipts = len(receipts)

        for receipt in receipts:
            for receipt_entry in receipt.entries:
                item = receipt_entry.item_name

                report_entries.setdefault(
                    item, ReportEntry(item=receipt_entry.item_name, units=0, revenue=0)
                )

                old_report_entry = report_entries[item]

                report_entries[item] = ReportEntry(
                    item=receipt_entry.item_name,
                    units=old_report_entry.units + receipt_entry.units,
                    revenue=old_report_entry.revenue + receipt_entry.total_price,
                )

        report = Report(
            entries=list(report_entries.values()),
            total_revenue=total_revenue,
            closed_receipts=closed_receipts,
            report_date=report_date,
        )

        if self.__report_repository.has_report(report_date=report_date):
            self.__report_repository.update_report(report, report_date=report_date)
        else:
            self.__report_repository.add_report(report, report_date=report_date)

        return report
