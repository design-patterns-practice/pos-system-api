from setuptools import setup

setup(
    name="POS System API",
    version="1.0.0",
    description="A simple POS System with an HTTP API.",
    package_dir={"": "app"},
    author="Zurab Mujirishvili",
    author_email="zmuji18@freeuni.edu.ge",
)
