from dataclasses import dataclass
from datetime import date
from typing import Final, List, Optional
from uuid import uuid4

import pytest
from core import Receipt, ReceiptEntry, ReceiptStatus
from infra import (
    CreateReportResponse,
    FetchReportResponse,
    InMemoryItemRepository,
    InMemoryReceiptRepository,
    ReportEntryModel,
    ReportModel,
)
from requests import Response
from response_utils import parse_response
from starlette import status
from starlette.testclient import TestClient

ItemEntry = InMemoryItemRepository.ItemEntry

ITEM_1: Final[ItemEntry] = ItemEntry(name="Item 1", price=2.5)
ITEM_2: Final[ItemEntry] = ItemEntry(name="Item 2", price=0.5)
ITEM_3: Final[ItemEntry] = ItemEntry(name="Item 3", price=5.0)


@dataclass
class WrappedTestClient:
    test_client: TestClient

    def fetch_report(self, *, report_date: Optional[str] = None) -> Response:
        """Submits a GET request to fetch the report for the current day."""
        return self.test_client.get(
            f"/reports/{report_date or date.today().isoformat()}"
        )

    def create_report(self, *, report_date: Optional[str] = None) -> Response:
        """Submits a PUT request to create a report for the current day."""
        return self.test_client.put(
            f"/reports/{report_date or date.today().isoformat()}"
        )


@pytest.fixture(autouse=True)
def preset_item_repository(
    in_memory_item_repository: InMemoryItemRepository,
) -> InMemoryItemRepository:
    """This will populate the IItemRepository that's used in the tests with some item entries."""
    in_memory_item_repository.add_items([ITEM_1, ITEM_2, ITEM_3])
    return in_memory_item_repository


@pytest.fixture
def client(test_client: TestClient) -> WrappedTestClient:
    return WrappedTestClient(test_client)


def expect(
    given: ReportModel,
    entries: List[ReportEntryModel],
    total_revenue: float,
    closed_receipts: int,
    report_date: Optional[str] = None,
) -> None:
    """Validates that the given report is as expected."""

    def key(entry: ReportEntryModel) -> str:
        return entry.item

    assert sorted(given.entries, key=key) == sorted(entries, key=key)
    assert given.total_revenue == total_revenue
    assert given.closed_receipts == closed_receipts
    assert given.report_date == (report_date or date.today().isoformat())


def test_should_create_empty_report(client: WrappedTestClient) -> None:
    create_response = client.create_report()
    report = parse_response(create_response, CreateReportResponse).report

    assert create_response.status_code == status.HTTP_201_CREATED
    expect(given=report, entries=[], total_revenue=0.0, closed_receipts=0)


def test_should_fetch_empty_report(client: WrappedTestClient) -> None:
    client.create_report()

    fetch_response = client.fetch_report()
    report = parse_response(fetch_response, FetchReportResponse).report

    assert fetch_response.status_code == status.HTTP_200_OK
    expect(given=report, entries=[], total_revenue=0.0, closed_receipts=0)


@pytest.mark.parametrize("other_reports", [True, False])
def test_should_not_fetch_non_existent_report(
    client: WrappedTestClient, other_reports: bool
) -> None:
    fetch_date = date(2020, 1, 1).isoformat()
    other_date = date(2020, 2, 1).isoformat()

    if other_reports:
        client.create_report(report_date=other_date)

    fetch_response = client.fetch_report(report_date=fetch_date)

    assert fetch_response.status_code == status.HTTP_404_NOT_FOUND


def test_should_create_report_with_one_receipt(
    client: WrappedTestClient, in_memory_receipt_repository: InMemoryReceiptRepository
) -> None:
    in_memory_receipt_repository.add_receipts(
        [
            Receipt(
                id=str(uuid4()),
                entries=[
                    ReceiptEntry(
                        item_name=ITEM_1.name,
                        unit_price=ITEM_1.price,
                        units=2,
                        total_price=ITEM_1.price * 2,
                    )
                ],
                total_price=ITEM_1.price * 2,
                status=ReceiptStatus.CLOSED,
                receipt_date=date.today(),
            )
        ]
    )

    create_response = client.create_report()
    report = parse_response(create_response, CreateReportResponse).report

    assert create_response.status_code == status.HTTP_201_CREATED
    expect(
        given=report,
        entries=[ReportEntryModel(item=ITEM_1.name, units=2, revenue=ITEM_1.price * 2)],
        total_revenue=ITEM_1.price * 2,
        closed_receipts=1,
    )


def test_should_create_report_with_multiple_receipts(
    client: WrappedTestClient, in_memory_receipt_repository: InMemoryReceiptRepository
) -> None:
    in_memory_receipt_repository.add_receipts(
        [
            Receipt(
                id=str(uuid4()),
                entries=[
                    ReceiptEntry(
                        item_name=ITEM_1.name,
                        unit_price=ITEM_1.price,
                        units=2,
                        total_price=ITEM_1.price * 2,
                    )
                ],
                total_price=ITEM_1.price * 2,
                status=ReceiptStatus.CLOSED,
                receipt_date=date.today(),
            ),
            Receipt(
                id=str(uuid4()),
                entries=[
                    ReceiptEntry(
                        item_name=ITEM_3.name,
                        unit_price=ITEM_3.price,
                        units=3,
                        total_price=ITEM_3.price * 3,
                    ),
                    ReceiptEntry(
                        item_name=ITEM_1.name,
                        unit_price=ITEM_1.price,
                        units=2,
                        total_price=ITEM_1.price * 2,
                    ),
                ],
                total_price=ITEM_3.price * 3 + ITEM_1.price * 2,
                status=ReceiptStatus.CLOSED,
                receipt_date=date.today(),
            ),
        ]
    )

    create_response = client.create_report()
    report = parse_response(create_response, CreateReportResponse).report

    assert create_response.status_code == status.HTTP_201_CREATED
    expect(
        given=report,
        entries=[
            ReportEntryModel(item=ITEM_1.name, units=4, revenue=ITEM_1.price * 4),
            ReportEntryModel(item=ITEM_3.name, units=3, revenue=ITEM_3.price * 3),
        ],
        total_revenue=ITEM_1.price * 4 + ITEM_3.price * 3,
        closed_receipts=2,
    )


def test_should_fetch_non_empty_report(
    client: WrappedTestClient, in_memory_receipt_repository: InMemoryReceiptRepository
) -> None:
    in_memory_receipt_repository.add_receipts(
        [
            Receipt(
                id=str(uuid4()),
                entries=[
                    ReceiptEntry(
                        item_name=ITEM_1.name,
                        unit_price=ITEM_1.price,
                        units=2,
                        total_price=ITEM_1.price * 2,
                    )
                ],
                total_price=ITEM_1.price * 2,
                status=ReceiptStatus.CLOSED,
                receipt_date=date.today(),
            )
        ]
    )

    client.create_report()
    fetch_response = client.fetch_report()
    report = parse_response(fetch_response, FetchReportResponse).report

    assert fetch_response.status_code == status.HTTP_200_OK
    expect(
        given=report,
        entries=[ReportEntryModel(item=ITEM_1.name, units=2, revenue=ITEM_1.price * 2)],
        total_revenue=ITEM_1.price * 2,
        closed_receipts=1,
    )


def test_should_fetch_report_after_a_receipt_was_added(
    client: WrappedTestClient, in_memory_receipt_repository: InMemoryReceiptRepository
) -> None:
    in_memory_receipt_repository.add_receipts(
        [
            Receipt(
                id=str(uuid4()),
                entries=[
                    ReceiptEntry(
                        item_name=ITEM_1.name,
                        unit_price=ITEM_1.price,
                        units=2,
                        total_price=ITEM_1.price * 2,
                    )
                ],
                total_price=ITEM_1.price * 2,
                status=ReceiptStatus.CLOSED,
                receipt_date=date.today(),
            )
        ]
    )

    # The Report is generated here.
    client.create_report()

    # Meanwhile a new receipt is created.
    in_memory_receipt_repository.add_receipts(
        [
            Receipt(
                id=str(uuid4()),
                entries=[
                    ReceiptEntry(
                        item_name=ITEM_3.name,
                        unit_price=ITEM_3.price,
                        units=3,
                        total_price=ITEM_3.price * 3,
                    )
                ],
                total_price=ITEM_3.price * 3,
                status=ReceiptStatus.CLOSED,
                receipt_date=date.today(),
            )
        ]
    )

    # And the report is fetched here.
    fetch_response = client.fetch_report()
    report = parse_response(fetch_response, FetchReportResponse).report

    assert fetch_response.status_code == status.HTTP_200_OK
    expect(
        given=report,
        entries=[ReportEntryModel(item=ITEM_1.name, units=2, revenue=ITEM_1.price * 2)],
        total_revenue=ITEM_1.price * 2,
        closed_receipts=1,
    )


def test_should_not_include_open_receipts_in_report(
    client: WrappedTestClient, in_memory_receipt_repository: InMemoryReceiptRepository
) -> None:
    in_memory_receipt_repository.add_receipts(
        [
            Receipt(
                id=str(uuid4()),
                entries=[
                    ReceiptEntry(
                        item_name=ITEM_1.name,
                        unit_price=ITEM_1.price,
                        units=2,
                        total_price=ITEM_1.price * 2,
                    )
                ],
                total_price=ITEM_1.price * 2,
                status=ReceiptStatus.CLOSED,
                receipt_date=date.today(),
            ),
            Receipt(
                id=str(uuid4()),
                entries=[
                    ReceiptEntry(
                        item_name=ITEM_2.name,
                        unit_price=ITEM_2.price,
                        units=3,
                        total_price=ITEM_2.price * 3,
                    )
                ],
                total_price=ITEM_2.price * 3,
                status=ReceiptStatus.OPEN,
                receipt_date=date.today(),
            ),
        ]
    )

    client.create_report()

    fetch_response = client.fetch_report()
    report = parse_response(fetch_response, FetchReportResponse).report

    assert fetch_response.status_code == status.HTTP_200_OK
    expect(
        given=report,
        entries=[ReportEntryModel(item=ITEM_1.name, units=2, revenue=ITEM_1.price * 2)],
        total_revenue=ITEM_1.price * 2,
        closed_receipts=1,
    )


def test_should_update_report(
    client: WrappedTestClient, in_memory_receipt_repository: InMemoryReceiptRepository
) -> None:
    in_memory_receipt_repository.add_receipts(
        [
            Receipt(
                id=str(uuid4()),
                entries=[
                    ReceiptEntry(
                        item_name=ITEM_1.name,
                        unit_price=ITEM_1.price,
                        units=2,
                        total_price=ITEM_1.price * 2,
                    )
                ],
                total_price=ITEM_1.price * 2,
                status=ReceiptStatus.CLOSED,
                receipt_date=date.today(),
            )
        ]
    )

    # The Report is generated here.
    client.create_report()

    # Meanwhile a new receipt is created.
    in_memory_receipt_repository.add_receipts(
        [
            Receipt(
                id=str(uuid4()),
                entries=[
                    ReceiptEntry(
                        item_name=ITEM_3.name,
                        unit_price=ITEM_3.price,
                        units=3,
                        total_price=ITEM_3.price * 3,
                    )
                ],
                total_price=ITEM_3.price * 3,
                status=ReceiptStatus.CLOSED,
                receipt_date=date.today(),
            )
        ]
    )

    # And the report is fetched here.
    create_response = client.create_report()
    report = parse_response(create_response, CreateReportResponse).report

    assert create_response.status_code == status.HTTP_201_CREATED
    expect(
        given=report,
        entries=[
            ReportEntryModel(item=ITEM_1.name, units=2, revenue=ITEM_1.price * 2),
            ReportEntryModel(item=ITEM_3.name, units=3, revenue=ITEM_3.price * 3),
        ],
        total_revenue=ITEM_1.price * 2 + ITEM_3.price * 3,
        closed_receipts=2,
    )


def test_should_create_report_for_different_dates(
    client: WrappedTestClient, in_memory_receipt_repository: InMemoryReceiptRepository
) -> None:
    date_1 = date(2020, 2, 2).isoformat()
    date_2 = date(2020, 2, 3).isoformat()

    in_memory_receipt_repository.add_receipts(
        [
            Receipt(
                id=str(uuid4()),
                entries=[
                    ReceiptEntry(
                        item_name=ITEM_1.name,
                        unit_price=ITEM_1.price,
                        units=1,
                        total_price=ITEM_1.price * 1,
                    )
                ],
                total_price=ITEM_1.price * 1,
                status=ReceiptStatus.CLOSED,
                receipt_date=date.fromisoformat(date_1),
            ),
            Receipt(
                id=str(uuid4()),
                entries=[
                    ReceiptEntry(
                        item_name=ITEM_3.name,
                        unit_price=ITEM_3.price,
                        units=3,
                        total_price=ITEM_3.price * 3,
                    ),
                    ReceiptEntry(
                        item_name=ITEM_1.name,
                        unit_price=ITEM_1.price,
                        units=2,
                        total_price=ITEM_1.price * 2,
                    ),
                ],
                total_price=ITEM_3.price * 3 + ITEM_1.price * 2,
                status=ReceiptStatus.CLOSED,
                receipt_date=date.fromisoformat(date_2),
            ),
        ]
    )

    create_response_1 = client.create_report(report_date=date_1)
    report_1 = parse_response(create_response_1, CreateReportResponse).report

    create_response_2 = client.create_report(report_date=date_2)
    report_2 = parse_response(create_response_2, CreateReportResponse).report

    assert (
        create_response_1.status_code
        == create_response_2.status_code
        == status.HTTP_201_CREATED
    )
    expect(
        given=report_1,
        entries=[ReportEntryModel(item=ITEM_1.name, units=1, revenue=ITEM_1.price * 1)],
        total_revenue=ITEM_1.price * 1,
        closed_receipts=1,
        report_date=date_1,
    )
    expect(
        given=report_2,
        entries=[
            ReportEntryModel(item=ITEM_1.name, units=2, revenue=ITEM_1.price * 2),
            ReportEntryModel(item=ITEM_3.name, units=3, revenue=ITEM_3.price * 3),
        ],
        total_revenue=ITEM_3.price * 3 + ITEM_1.price * 2,
        closed_receipts=1,
        report_date=date_2,
    )
