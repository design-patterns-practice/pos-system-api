import pytest
from infra import (
    InMemoryItemRepository,
    InMemoryReceiptRepository,
    InMemoryReportRepository,
)
from runner.web.setup import setup
from starlette.testclient import TestClient


@pytest.fixture
def in_memory_item_repository() -> InMemoryItemRepository:
    return InMemoryItemRepository()


@pytest.fixture
def in_memory_receipt_repository() -> InMemoryReceiptRepository:
    return InMemoryReceiptRepository()


@pytest.fixture
def in_memory_report_repository() -> InMemoryReportRepository:
    return InMemoryReportRepository()


@pytest.fixture
def test_client(
    in_memory_item_repository: InMemoryItemRepository,
    in_memory_receipt_repository: InMemoryReceiptRepository,
    in_memory_report_repository: InMemoryReportRepository,
) -> TestClient:
    return TestClient(
        setup(
            item_repository=in_memory_item_repository,
            receipt_repository=in_memory_receipt_repository,
            report_repository=in_memory_report_repository,
        )
    )
