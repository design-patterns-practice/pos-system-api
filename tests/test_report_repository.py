from datetime import date
from typing import Any, Final, Iterator, List, Optional

from core import IReportRepository, Report, ReportEntry
from infra import (
    InMemoryConnectionFactory,
    InMemoryReportRepository,
    SQLiteReportRepository,
)
from pytest_cases import fixture, parametrize


@fixture  # type: ignore
def in_memory_report_repository() -> InMemoryReportRepository:
    repository = InMemoryReportRepository()

    return repository


@fixture  # type: ignore
def sqlite_report_repository() -> Iterator[SQLiteReportRepository]:
    with SQLiteReportRepository(
        connection_factory=InMemoryConnectionFactory(),
        init_files=["data_base/reports.sql"],
    ) as repository:
        yield repository


REPORT_REPOSITORIES: Final[Any] = (
    in_memory_report_repository,
    sqlite_report_repository,
)


def expect(
    given: Report,
    entries: List[ReportEntry],
    total_revenue: float,
    closed_receipts: int,
    report_date: Optional[date] = None,
) -> None:
    """Validates that the given report is as expected."""

    def key(entry: ReportEntry) -> str:
        return entry.item

    assert sorted(given.entries, key=key) == sorted(entries, key=key)
    assert given.total_revenue == total_revenue
    assert given.closed_receipts == closed_receipts
    assert given.report_date == (report_date or date.today())


@parametrize("report_repository", REPORT_REPOSITORIES)  # type: ignore
def test_should_create_receipt_repository(
    report_repository: IReportRepository,
) -> None:
    assert report_repository is not None


@parametrize("report_repository", REPORT_REPOSITORIES)  # type: ignore
def test_should_add_empty_report(
    report_repository: IReportRepository,
) -> None:
    report = Report(
        entries=[],
        total_revenue=0.0,
        closed_receipts=0,
        report_date=date.today(),
    )

    report_repository.add_report(report, report_date=date.today())


@parametrize("report_repository", REPORT_REPOSITORIES)  # type: ignore
def test_should_retrieve_empty_report(
    report_repository: IReportRepository,
) -> None:
    report = Report(
        entries=[],
        total_revenue=0.0,
        closed_receipts=0,
        report_date=date.today(),
    )

    report_repository.add_report(report, report_date=date.today())
    expect(
        given=report_repository.get_report(report_date=date.today()),
        entries=[],
        total_revenue=0.0,
        closed_receipts=0,
        report_date=date.today(),
    )


@parametrize("report_repository", REPORT_REPOSITORIES)  # type: ignore
def test_should_add_non_empty_report(
    report_repository: IReportRepository,
) -> None:
    report = Report(
        entries=[ReportEntry(item="Item 1", units=2, revenue=2.5)],
        total_revenue=0.5,
        closed_receipts=2,
        report_date=date.today(),
    )

    report_repository.add_report(report, report_date=date.today())


@parametrize("report_repository", REPORT_REPOSITORIES)  # type: ignore
def test_should_retrieve_non_empty_report(
    report_repository: IReportRepository,
) -> None:
    report_date = date(2021, 11, 2)
    report = Report(
        entries=[ReportEntry(item="Item 1", units=2, revenue=2.5)],
        total_revenue=0.5,
        closed_receipts=2,
        report_date=report_date,
    )

    report_repository.add_report(report, report_date=report_date)

    expect(
        given=report_repository.get_report(report_date=report_date),
        entries=[ReportEntry(item="Item 1", units=2, revenue=2.5)],
        total_revenue=0.5,
        closed_receipts=2,
        report_date=report_date,
    )


@parametrize("report_repository", REPORT_REPOSITORIES)  # type: ignore
def test_should_find_reports(
    report_repository: IReportRepository,
) -> None:
    report_dates = [date(2021, 11, 3), date(2021, 2, 2), date(2025, 11, 2)]
    for report_date in report_dates:
        report = Report(
            entries=[ReportEntry(item="Item 1", units=2, revenue=2.5)],
            total_revenue=0.5,
            closed_receipts=2,
            report_date=report_date,
        )
        report_repository.add_report(report, report_date=report_date)

    for report_date in report_dates:
        assert report_repository.has_report(report_date=report_date) is True


@parametrize("report_repository", REPORT_REPOSITORIES)  # type: ignore
def test_should_not_find_reports(
    report_repository: IReportRepository,
) -> None:
    report_dates = [date(2021, 11, 3), date(2021, 2, 2), date(2025, 11, 2)]
    other_dates = [date(2023, 11, 3), date(2021, 5, 2), date(2025, 11, 7)]
    for report_date in report_dates:
        report = Report(
            entries=[ReportEntry(item="Item 1", units=2, revenue=2.5)],
            total_revenue=0.5,
            closed_receipts=2,
            report_date=report_date,
        )
        report_repository.add_report(report, report_date=report_date)

    for report_date in other_dates:
        assert report_repository.has_report(report_date=report_date) is False


@parametrize("report_repository", REPORT_REPOSITORIES)  # type: ignore
def test_should_add_multiple_reports(
    report_repository: IReportRepository,
) -> None:
    report_1 = Report(
        entries=[ReportEntry(item="Item 1", units=2, revenue=2.5)],
        total_revenue=0.0,
        closed_receipts=4,
        report_date=date(2020, 1, 2),
    )
    report_2 = Report(
        entries=[
            ReportEntry(item="Item 1", units=2, revenue=2.5),
            ReportEntry(item="Item 2", units=4, revenue=25.0),
        ],
        total_revenue=0.0,
        closed_receipts=2,
        report_date=date(2020, 2, 3),
    )

    report_repository.add_report(report_1, report_date=report_1.report_date)
    report_repository.add_report(report_2, report_date=report_2.report_date)


@parametrize("report_repository", REPORT_REPOSITORIES)  # type: ignore
def test_should_update_report(
    report_repository: IReportRepository,
) -> None:
    report = Report(
        entries=[
            ReportEntry(item="Item 1", units=2, revenue=2.5),
            ReportEntry(item="Item 2", units=3, revenue=2.0),
        ],
        total_revenue=0.0,
        closed_receipts=4,
        report_date=date.today(),
    )

    updated_report = Report(
        entries=[
            ReportEntry(item="Item 2", units=4, revenue=0.5),
            ReportEntry(item="Item 3", units=4, revenue=25.0),
        ],
        total_revenue=0.0,
        closed_receipts=2,
        report_date=date.today(),
    )

    report_repository.add_report(report, report_date=report.report_date)
    report_repository.update_report(
        updated_report, report_date=updated_report.report_date
    )

    expect(
        given=report_repository.get_report(report_date=updated_report.report_date),
        entries=updated_report.entries,
        total_revenue=updated_report.total_revenue,
        closed_receipts=updated_report.closed_receipts,
        report_date=updated_report.report_date,
    )
