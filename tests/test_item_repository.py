from dataclasses import dataclass
from typing import Any, Final, Iterator

from core import IItemRepository
from infra import (
    InMemoryConnectionFactory,
    InMemoryItemRepository,
    SQLiteItemRepository,
)
from pytest_cases import fixture, parametrize


@dataclass
class Item:
    name: str
    price: float

    def to_in_memory_entry(self) -> InMemoryItemRepository.ItemEntry:
        return InMemoryItemRepository.ItemEntry(name=self.name, price=self.price)

    def to_sqlite_entry(self) -> SQLiteItemRepository.ItemEntry:
        return SQLiteItemRepository.ItemEntry(name=self.name, price=self.price)


ITEM_1: Final[Item] = Item("Item 1", 2.5)
ITEM_2: Final[Item] = Item("Item 2", 0.5)
ITEM_3: Final[Item] = Item("Item 3", 5.0)


@fixture  # type: ignore
def in_memory_item_repository() -> InMemoryItemRepository:
    repository = InMemoryItemRepository()
    repository.add_items(
        [
            ITEM_1.to_in_memory_entry(),
            ITEM_2.to_in_memory_entry(),
            ITEM_3.to_in_memory_entry(),
        ]
    )

    return repository


@fixture  # type: ignore
def sqlite_item_repository() -> Iterator[SQLiteItemRepository]:
    with SQLiteItemRepository(
        connection_factory=InMemoryConnectionFactory(),
        init_files=["data_base/items.sql"],
    ) as repository:
        repository.add_items(
            [
                ITEM_1.to_sqlite_entry(),
                ITEM_2.to_sqlite_entry(),
                ITEM_3.to_sqlite_entry(),
            ]
        )

        yield repository


ITEM_REPOSITORIES: Final[Any] = (
    in_memory_item_repository,
    sqlite_item_repository,
)


@parametrize("item_repository", ITEM_REPOSITORIES)  # type: ignore
def test_should_create_item_repository(item_repository: IItemRepository) -> None:
    assert item_repository is not None


@parametrize("item_repository", ITEM_REPOSITORIES)  # type: ignore
def test_should_retrieve_one_item_price(item_repository: IItemRepository) -> None:
    assert item_repository.get_item_price(ITEM_1.name) == ITEM_1.price


@parametrize("item_repository", ITEM_REPOSITORIES)  # type: ignore
def test_should_retrieve_multiple_item_prices(item_repository: IItemRepository) -> None:
    assert item_repository.get_item_price(ITEM_1.name) == ITEM_1.price
    assert item_repository.get_item_price(ITEM_2.name) == ITEM_2.price
    assert item_repository.get_item_price(ITEM_3.name) == ITEM_3.price


@parametrize("item_repository", ITEM_REPOSITORIES)  # type: ignore
def test_should_retrieve_item_price_multiple_times(
    item_repository: IItemRepository,
) -> None:
    assert item_repository.get_item_price(ITEM_1.name) == ITEM_1.price
    assert item_repository.get_item_price(ITEM_1.name) == ITEM_1.price
    assert item_repository.get_item_price(ITEM_1.name) == ITEM_1.price


@parametrize("item_repository", ITEM_REPOSITORIES)  # type: ignore
def test_should_find_items(
    item_repository: IItemRepository,
) -> None:
    assert item_repository.has_item(ITEM_1.name) is True
    assert item_repository.has_item(ITEM_2.name) is True
    assert item_repository.has_item(ITEM_3.name) is True


@parametrize("item_repository", ITEM_REPOSITORIES)  # type: ignore
def test_should_not_find_items(
    item_repository: IItemRepository,
) -> None:
    assert item_repository.has_item(f"Not {ITEM_1.name}") is False
    assert item_repository.has_item(f"Not {ITEM_2.name}") is False
    assert item_repository.has_item(f"Not {ITEM_3.name}") is False
