from datetime import date
from typing import Any, Final, Iterator, Optional, Sequence

from core import IReceiptRepository, Receipt, ReceiptEntry, ReceiptStatus
from infra import (
    InMemoryConnectionFactory,
    InMemoryReceiptRepository,
    SQLiteReceiptRepository,
)
from pytest_cases import fixture, parametrize


@fixture  # type: ignore
def in_memory_receipt_repository() -> InMemoryReceiptRepository:
    repository = InMemoryReceiptRepository()

    return repository


@fixture  # type: ignore
def sqlite_receipt_repository() -> Iterator[SQLiteReceiptRepository]:
    with SQLiteReceiptRepository(
        connection_factory=InMemoryConnectionFactory(),
        init_files=["data_base/receipts.sql"],
    ) as repository:
        yield repository


RECEIPT_REPOSITORIES: Final[Any] = (
    in_memory_receipt_repository,
    sqlite_receipt_repository,
)


def expect(
    given: Receipt,
    receipt_id: str,
    entries: Sequence[ReceiptEntry],
    total_price: float,
    receipt_status: ReceiptStatus,
    receipt_date: Optional[date] = None,
) -> None:
    """Validates that the given receipt is as expected."""

    def key(entry: ReceiptEntry) -> str:
        return entry.item_name

    assert given.id == receipt_id
    assert sorted(given.entries, key=key) == sorted(entries, key=key)
    assert given.total_price == total_price
    assert given.status == receipt_status
    assert given.receipt_date == (receipt_date or date.today())


@parametrize("receipt_repository", RECEIPT_REPOSITORIES)  # type: ignore
def test_should_create_receipt_repository(
    receipt_repository: IReceiptRepository,
) -> None:
    assert receipt_repository is not None


@parametrize("receipt_repository", RECEIPT_REPOSITORIES)  # type: ignore
def test_should_create_receipts(receipt_repository: IReceiptRepository) -> None:
    assert receipt_repository.create_receipt(receipt_date=date.today()) != ""


@parametrize("receipt_repository", RECEIPT_REPOSITORIES)  # type: ignore
def test_should_create_multiple_receipts(
    receipt_repository: IReceiptRepository,
) -> None:
    receipt_id_1 = receipt_repository.create_receipt(receipt_date=date.today())
    receipt_id_2 = receipt_repository.create_receipt(receipt_date=date.today())

    assert receipt_id_1 != receipt_id_2 != ""


@parametrize("receipt_repository", RECEIPT_REPOSITORIES)  # type: ignore
def test_should_retrieve_receipt(receipt_repository: IReceiptRepository) -> None:
    receipt_id = receipt_repository.create_receipt(receipt_date=date.today())

    expect(
        given=receipt_repository.get_receipt(receipt_id),
        receipt_id=receipt_id,
        entries=[],
        total_price=0.0,
        receipt_status=ReceiptStatus.OPEN,
    )


@parametrize("receipt_repository", RECEIPT_REPOSITORIES)  # type: ignore
def test_should_retrieve_multiple_receipts(
    receipt_repository: IReceiptRepository,
) -> None:
    receipt_id_1 = receipt_repository.create_receipt(receipt_date=date.today())
    receipt_id_2 = receipt_repository.create_receipt(receipt_date=date.today())

    expect(
        given=receipt_repository.get_receipt(receipt_id_2),
        receipt_id=receipt_id_2,
        entries=[],
        total_price=0.0,
        receipt_status=ReceiptStatus.OPEN,
    )
    expect(
        given=receipt_repository.get_receipt(receipt_id_1),
        receipt_id=receipt_id_1,
        entries=[],
        total_price=0.0,
        receipt_status=ReceiptStatus.OPEN,
    )


@parametrize("receipt_repository", RECEIPT_REPOSITORIES)  # type: ignore
def test_should_retrieve_receipts_for_given_date(
    receipt_repository: IReceiptRepository,
) -> None:
    date_1 = date(2020, 1, 2)
    date_2 = date(2020, 1, 1)

    receipt_id_1 = receipt_repository.create_receipt(receipt_date=date_1)
    receipt_id_2 = receipt_repository.create_receipt(receipt_date=date_1)

    receipt_repository.create_receipt(receipt_date=date_2)

    receipts = receipt_repository.get_receipts(receipt_date=date_1)
    receipt_1 = [receipt for receipt in receipts if receipt.id == receipt_id_1][0]
    receipt_2 = [receipt for receipt in receipts if receipt.id == receipt_id_2][0]

    assert len(receipts) == 2
    expect(
        given=receipt_1,
        receipt_id=receipt_id_1,
        entries=[],
        total_price=0.0,
        receipt_status=ReceiptStatus.OPEN,
        receipt_date=date_1,
    )
    expect(
        given=receipt_2,
        receipt_id=receipt_id_2,
        entries=[],
        total_price=0.0,
        receipt_status=ReceiptStatus.OPEN,
        receipt_date=date_1,
    )


@parametrize("receipt_repository", RECEIPT_REPOSITORIES)  # type: ignore
def test_should_find_receipts(receipt_repository: IReceiptRepository) -> None:
    receipt_id_1 = receipt_repository.create_receipt(receipt_date=date.today())
    receipt_id_2 = receipt_repository.create_receipt(receipt_date=date.today())

    assert receipt_repository.has_receipt(receipt_id_1) is True
    assert receipt_repository.has_receipt(receipt_id_2) is True


@parametrize("receipt_repository", RECEIPT_REPOSITORIES)  # type: ignore
def test_should_not_find_receipts(receipt_repository: IReceiptRepository) -> None:
    receipt_id_1 = receipt_repository.create_receipt(receipt_date=date.today())
    receipt_id_2 = receipt_repository.create_receipt(receipt_date=date.today())

    assert receipt_repository.has_receipt(f"Not {receipt_id_1}") is False
    assert receipt_repository.has_receipt(f"Not {receipt_id_2}") is False


@parametrize("receipt_repository", RECEIPT_REPOSITORIES)  # type: ignore
def test_should_update_receipt(receipt_repository: IReceiptRepository) -> None:
    receipt_date = date(2024, 5, 12)
    receipt_entries = [  # The consistency between price/units don't matter here.
        ReceiptEntry(item_name="Item 1", unit_price=1.2, units=0, total_price=1.0),
        ReceiptEntry(item_name="Item 2", unit_price=0.2, units=20, total_price=0.4),
        ReceiptEntry(item_name="Item 3", unit_price=12.0, units=2, total_price=12.0),
    ]

    receipt_id = receipt_repository.create_receipt(receipt_date=date.today())

    receipt_repository.update_receipt(
        receipt_id=receipt_id,
        receipt=Receipt(
            id=receipt_id,
            entries=receipt_entries,
            total_price=2.5,
            receipt_date=receipt_date,
            status=ReceiptStatus.CLOSED,
        ),
    )

    expect(
        given=receipt_repository.get_receipt(receipt_id),
        receipt_id=receipt_id,
        entries=receipt_entries,
        total_price=2.5,
        receipt_date=receipt_date,
        receipt_status=ReceiptStatus.CLOSED,
    )


@parametrize("receipt_repository", RECEIPT_REPOSITORIES)  # type: ignore
def test_should_update_receipt_multiple_times(
    receipt_repository: IReceiptRepository,
) -> None:
    receipt_date = date(2024, 5, 12)
    changed_date = date(2020, 5, 12)
    receipt_entries = [  # The consistency between price/units don't matter here.
        ReceiptEntry(item_name="Item 1", unit_price=1.2, units=0, total_price=1.0),
        ReceiptEntry(item_name="Item 2", unit_price=0.2, units=20, total_price=0.4),
        ReceiptEntry(item_name="Item 3", unit_price=12.0, units=2, total_price=12.0),
    ]
    new_entries = [
        ReceiptEntry(item_name="Item 4", unit_price=3.2, units=12, total_price=5.0),
    ]

    receipt_id = receipt_repository.create_receipt(receipt_date=date.today())
    receipt_repository.update_receipt(
        receipt_id=receipt_id,
        receipt=Receipt(
            id=receipt_id,
            entries=receipt_entries,
            total_price=2.5,
            receipt_date=receipt_date,
            status=ReceiptStatus.CLOSED,
        ),
    )
    receipt_repository.update_receipt(
        receipt_id=receipt_id,
        receipt=Receipt(
            id=receipt_id,
            entries=receipt_entries[:-2] + new_entries,
            total_price=0.5,
            receipt_date=changed_date,
            status=ReceiptStatus.OPEN,
        ),
    )

    expect(
        given=receipt_repository.get_receipt(receipt_id),
        receipt_id=receipt_id,
        entries=receipt_entries[:-2] + new_entries,
        total_price=0.5,
        receipt_date=changed_date,
        receipt_status=ReceiptStatus.OPEN,
    )
