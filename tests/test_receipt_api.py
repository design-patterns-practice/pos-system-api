from dataclasses import dataclass
from datetime import date
from typing import Dict, Sequence, Tuple
from uuid import uuid4

import pytest
from core import ReceiptStatus
from infra import (
    CloseReceiptResponse,
    CreateReceiptResponse,
    FetchReceiptResponse,
    InMemoryItemRepository,
    ReceiptEntryModel,
    ReceiptModel,
    UpdateReceiptResponse,
)
from requests import Response
from response_utils import parse_response
from starlette import status
from starlette.testclient import TestClient

ItemEntry = InMemoryItemRepository.ItemEntry


@dataclass
class WrappedTestClient:
    test_client: TestClient

    def fetch_receipt(self, receipt_id: str) -> Response:
        """Sends a GET request to retrieve a receipt."""
        return self.test_client.get(f"/receipts/{receipt_id}")

    def create_receipt(self) -> Response:
        """Sends a POST request to create a receipt."""
        return self.test_client.post("/receipts")

    def update_receipt(self, receipt_id: str, *, items: Dict[str, int]) -> Response:
        """Sends a PUT request to update the receipt with the specified ID."""
        return self.test_client.put(f"/receipts/{receipt_id}/items", json=items)

    def close_receipt(self, receipt_id: str) -> Response:
        """Sends a PUT request to close the receipt with the specified ID."""
        return self.test_client.put(f"/receipts/{receipt_id}")

    def create_and_update_receipt(
        self, *, items: Dict[str, int]
    ) -> Tuple[str, Response, ReceiptModel]:
        """Creates a receipt and updates it with the specified items.

        :returns The receipt ID, the update response and the receipt."""
        receipt_id = parse_response(
            self.create_receipt(), CreateReceiptResponse
        ).receipt_id

        response = self.update_receipt(receipt_id, items=items)
        receipt = parse_response(response, UpdateReceiptResponse).receipt

        return receipt_id, response, receipt


@pytest.fixture
def client(test_client: TestClient) -> WrappedTestClient:
    return WrappedTestClient(test_client)


def expect(
    given: ReceiptModel,
    receipt_id: str,
    entries: Sequence[ReceiptEntryModel],
    total_price: float,
    receipt_status: ReceiptStatus = ReceiptStatus.OPEN,
) -> None:
    """Validates that the given receipt is as expected."""

    def key(entry: ReceiptEntryModel) -> str:
        return entry.item_name

    assert given.id == receipt_id
    assert sorted(given.entries, key=key) == sorted(entries, key=key)
    assert given.total_price == total_price
    assert given.status == receipt_status
    assert given.receipt_date == date.today().isoformat()


def test_should_create_receipt(client: WrappedTestClient) -> None:
    response = client.create_receipt()
    content = parse_response(response, CreateReceiptResponse)

    assert response.status_code == status.HTTP_201_CREATED
    assert content.receipt_id != ""


def test_should_create_multiple_receipts(client: WrappedTestClient) -> None:
    response_1 = client.create_receipt()
    response_2 = client.create_receipt()

    receipt_id_1 = parse_response(response_1, CreateReceiptResponse).receipt_id
    receipt_id_2 = parse_response(response_2, CreateReceiptResponse).receipt_id

    assert response_1.status_code == status.HTTP_201_CREATED
    assert response_2.status_code == status.HTTP_201_CREATED
    assert receipt_id_1 != receipt_id_2 != ""


def test_should_fetch_receipt(client: WrappedTestClient) -> None:
    receipt_id = parse_response(
        client.create_receipt(), CreateReceiptResponse
    ).receipt_id

    response = client.fetch_receipt(receipt_id)
    content = parse_response(response, FetchReceiptResponse)

    assert response.status_code == status.HTTP_200_OK
    expect(
        given=content.receipt,
        receipt_id=receipt_id,
        entries=[],
        total_price=0,
    )


@pytest.mark.parametrize("other_receipts", [True, False])
def test_should_not_fetch_non_existent_receipt(
    client: WrappedTestClient, other_receipts: bool
) -> None:
    if other_receipts:
        client.create_receipt()

    response = client.fetch_receipt(receipt_id=str(uuid4()))

    assert response.status_code == status.HTTP_404_NOT_FOUND


def test_should_update_receipt(
    client: WrappedTestClient, in_memory_item_repository: InMemoryItemRepository
) -> None:
    in_memory_item_repository.add_items(
        [
            ItemEntry(name="Item 1", price=10.5),
            ItemEntry(name="Item 2", price=12.1),
            ItemEntry(name="Item 3", price=0.5),
        ]
    )

    receipt_id, response, receipt = client.create_and_update_receipt(
        items={"Item 1": 1, "Item 2": 3, "Item 3": 2}
    )

    assert response.status_code == status.HTTP_200_OK
    expect(
        given=receipt,
        receipt_id=receipt_id,
        entries=[
            ReceiptEntryModel(
                item_name="Item 1", unit_price=10.5, units=1, total_price=10.5
            ),
            ReceiptEntryModel(
                item_name="Item 2", unit_price=12.1, units=3, total_price=36.3
            ),
            ReceiptEntryModel(
                item_name="Item 3", unit_price=0.5, units=2, total_price=1.0
            ),
        ],
        total_price=47.8,
    )


@pytest.mark.parametrize("other_receipts", [True, False])
def test_should_not_update_non_existent_receipt(
    client: WrappedTestClient,
    in_memory_item_repository: InMemoryItemRepository,
    other_receipts: bool,
) -> None:
    if other_receipts:
        client.create_receipt()

    in_memory_item_repository.add_items([ItemEntry(name="Item 1", price=10.5)])

    response = client.update_receipt(receipt_id=str(uuid4()), items={"Item 1": 2})

    assert response.status_code == status.HTTP_404_NOT_FOUND


def test_should_update_multiple_receipts(
    client: WrappedTestClient, in_memory_item_repository: InMemoryItemRepository
) -> None:
    in_memory_item_repository.add_items(
        [
            ItemEntry(name="Item 1", price=10.5),
            ItemEntry(name="Item 2", price=12.1),
            ItemEntry(name="Item 3", price=0.5),
        ]
    )

    receipt_id_1 = parse_response(
        client.create_receipt(), CreateReceiptResponse
    ).receipt_id

    # Meanwhile a different receipt is created & updated.
    receipt_id_2, update_response_2, receipt_2 = client.create_and_update_receipt(
        items={"Item 1": 2, "Item 2": 4}
    )

    # Back to the first one.
    update_response_1 = client.update_receipt(receipt_id_1, items={"Item 2": 2})
    receipt_1 = parse_response(update_response_1, UpdateReceiptResponse).receipt

    assert (
        update_response_1.status_code
        == update_response_2.status_code
        == status.HTTP_200_OK
    )
    expect(
        given=receipt_1,
        receipt_id=receipt_id_1,
        entries=[
            ReceiptEntryModel(
                item_name="Item 2", unit_price=12.1, units=2, total_price=24.2
            ),
        ],
        total_price=24.2,
    )
    expect(
        given=receipt_2,
        receipt_id=receipt_id_2,
        entries=[
            ReceiptEntryModel(
                item_name="Item 1", unit_price=10.5, units=2, total_price=21.0
            ),
            ReceiptEntryModel(
                item_name="Item 2", unit_price=12.1, units=4, total_price=48.4
            ),
        ],
        total_price=69.4,
    )


def test_should_update_receipt_multiple_times(
    client: WrappedTestClient, in_memory_item_repository: InMemoryItemRepository
) -> None:
    in_memory_item_repository.add_items(
        [
            ItemEntry(name="Item 1", price=10.5),
            ItemEntry(name="Item 2", price=12.1),
            ItemEntry(name="Item 3", price=0.5),
        ]
    )

    receipt_id, _, _ = client.create_and_update_receipt(
        items={"Item 1": 2, "Item 2": 4}
    )

    update_response = client.update_receipt(
        receipt_id, items={"Item 1": 1, "Item 3": 2}
    )
    receipt = parse_response(update_response, UpdateReceiptResponse).receipt

    assert update_response.status_code == status.HTTP_200_OK
    expect(
        given=receipt,
        receipt_id=receipt_id,
        entries=[
            ReceiptEntryModel(
                item_name="Item 1", unit_price=10.5, units=1, total_price=10.5
            ),
            ReceiptEntryModel(
                item_name="Item 3", unit_price=0.5, units=2, total_price=1.0
            ),
        ],
        total_price=11.5,
    )


def test_should_fetch_updated_receipt(
    client: WrappedTestClient, in_memory_item_repository: InMemoryItemRepository
) -> None:
    in_memory_item_repository.add_items(
        [
            ItemEntry(name="Item 1", price=10.5),
            ItemEntry(name="Item 2", price=12.1),
            ItemEntry(name="Item 3", price=0.5),
        ]
    )

    receipt_id, _, _ = client.create_and_update_receipt(
        items={"Item 1": 2, "Item 2": 4}
    )
    fetch_response = client.fetch_receipt(receipt_id)

    assert fetch_response.status_code == status.HTTP_200_OK
    expect(
        given=parse_response(fetch_response, FetchReceiptResponse).receipt,
        receipt_id=receipt_id,
        entries=[
            ReceiptEntryModel(
                item_name="Item 1", unit_price=10.5, units=2, total_price=21.0
            ),
            ReceiptEntryModel(
                item_name="Item 2", unit_price=12.1, units=4, total_price=48.4
            ),
        ],
        total_price=69.4,
    )


def test_should_close_empty_receipt(client: WrappedTestClient) -> None:
    receipt_id = parse_response(
        client.create_receipt(), CreateReceiptResponse
    ).receipt_id
    close_response = client.close_receipt(receipt_id)

    assert close_response.status_code == status.HTTP_200_OK
    expect(
        given=parse_response(close_response, CloseReceiptResponse).receipt,
        receipt_id=receipt_id,
        entries=[],
        total_price=0.0,
        receipt_status=ReceiptStatus.CLOSED,
    )


def test_should_close_updated_receipt(
    client: WrappedTestClient, in_memory_item_repository: InMemoryItemRepository
) -> None:
    in_memory_item_repository.add_items(
        [
            ItemEntry(name="Item", price=1.0),
            ItemEntry(name="Another Item", price=2.5),
        ]
    )
    receipt_id, _, _ = client.create_and_update_receipt(items={"Item": 3})
    close_response = client.close_receipt(receipt_id)

    assert close_response.status_code == status.HTTP_200_OK
    expect(
        given=parse_response(close_response, CloseReceiptResponse).receipt,
        receipt_id=receipt_id,
        entries=[
            ReceiptEntryModel(
                item_name="Item", unit_price=1.0, units=3, total_price=3.0
            )
        ],
        total_price=3.0,
        receipt_status=ReceiptStatus.CLOSED,
    )


def test_should_fetch_closed_receipt(client: WrappedTestClient) -> None:
    receipt_id = parse_response(
        client.create_receipt(), CreateReceiptResponse
    ).receipt_id
    client.close_receipt(receipt_id)

    fetch_response = client.fetch_receipt(receipt_id)
    receipt = parse_response(fetch_response, FetchReceiptResponse).receipt

    assert fetch_response.status_code == status.HTTP_200_OK
    expect(
        given=receipt,
        receipt_id=receipt_id,
        entries=[],
        total_price=0,
        receipt_status=ReceiptStatus.CLOSED,
    )


@pytest.mark.parametrize("other_receipts", [True, False])
def test_should_not_close_non_existent_receipt(
    client: WrappedTestClient,
    other_receipts: bool,
) -> None:
    if other_receipts:
        client.create_receipt()

    response = client.close_receipt(receipt_id=str(uuid4()))

    assert response.status_code == status.HTTP_404_NOT_FOUND


def test_should_not_update_closed_receipt(
    client: WrappedTestClient, in_memory_item_repository: InMemoryItemRepository
) -> None:
    in_memory_item_repository.add_items(
        [
            ItemEntry(name="Toothbrush", price=0.5),
            ItemEntry(name="Regular Brush", price=1.5),
        ]
    )

    receipt_id, _, _ = client.create_and_update_receipt(items={"Toothbrush": 1})
    client.close_receipt(receipt_id)

    update_response = client.update_receipt(receipt_id, items={"Regular Brush": 1})
    receipt = parse_response(
        client.fetch_receipt(receipt_id), FetchReceiptResponse
    ).receipt

    assert update_response.status_code == status.HTTP_403_FORBIDDEN
    expect(
        given=receipt,
        receipt_id=receipt_id,
        entries=[
            ReceiptEntryModel(
                item_name="Toothbrush", unit_price=0.5, units=1, total_price=0.5
            )
        ],
        total_price=0.5,
        receipt_status=ReceiptStatus.CLOSED,
    )


def test_should_not_close_closed_receipt(
    client: WrappedTestClient, in_memory_item_repository: InMemoryItemRepository
) -> None:
    in_memory_item_repository.add_items(
        [
            ItemEntry(name="Toothbrush", price=0.5),
            ItemEntry(name="Regular Brush", price=1.5),
        ]
    )

    receipt_id, _, _ = client.create_and_update_receipt(items={"Toothbrush": 1})
    client.close_receipt(receipt_id)

    close_response = client.close_receipt(receipt_id)
    receipt = parse_response(
        client.fetch_receipt(receipt_id), FetchReceiptResponse
    ).receipt

    assert close_response.status_code == status.HTTP_403_FORBIDDEN
    expect(
        given=receipt,
        receipt_id=receipt_id,
        entries=[
            ReceiptEntryModel(
                item_name="Toothbrush", unit_price=0.5, units=1, total_price=0.5
            )
        ],
        total_price=0.5,
        receipt_status=ReceiptStatus.CLOSED,
    )


def test_should_not_update_receipt_with_non_existent_item(
    client: WrappedTestClient, in_memory_item_repository: InMemoryItemRepository
) -> None:
    in_memory_item_repository.add_items(
        [
            ItemEntry(name="Item 1", price=10.5),
            ItemEntry(name="Item 3", price=0.5),
        ]
    )

    receipt_id = parse_response(
        client.create_receipt(), CreateReceiptResponse
    ).receipt_id

    update_response = client.update_receipt(
        receipt_id, items={"Item 1": 1, "Item 2": 3, "Item 3": 2}
    )

    receipt = parse_response(
        client.fetch_receipt(receipt_id), FetchReceiptResponse
    ).receipt

    assert update_response.status_code == status.HTTP_404_NOT_FOUND
    expect(
        given=receipt,
        receipt_id=receipt_id,
        entries=[],
        total_price=0.0,
    )


def test_should_not_update_receipt_with_invalid_amounts(
    client: WrappedTestClient, in_memory_item_repository: InMemoryItemRepository
) -> None:
    in_memory_item_repository.add_items(
        [
            ItemEntry(name="Item 1", price=10.5),
            ItemEntry(name="Item 2", price=2.6),
            ItemEntry(name="Item 3", price=0.5),
        ]
    )

    receipt_id = parse_response(
        client.create_receipt(), CreateReceiptResponse
    ).receipt_id

    # This is a valid update.
    client.update_receipt(receipt_id, items={"Item 1": 1, "Item 2": 2})

    # These are invalid.
    update_response_1 = client.update_receipt(
        receipt_id, items={"Item 1": 1, "Item 2": 1, "Item 3": -2}
    )
    update_response_2 = client.update_receipt(
        receipt_id, items={"Item 2": 0, "Item 3": 2}
    )
    update_response_3 = client.update_receipt(
        receipt_id, items={"Item 2": 2, "Item 3": -2, "Item  1": -1}
    )

    receipt = parse_response(
        client.fetch_receipt(receipt_id), FetchReceiptResponse
    ).receipt

    assert update_response_1.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY
    assert update_response_2.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY
    assert update_response_3.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY
    expect(
        given=receipt,
        receipt_id=receipt_id,
        entries=[
            ReceiptEntryModel(
                item_name="Item 1", unit_price=10.5, units=1, total_price=10.5
            ),
            ReceiptEntryModel(
                item_name="Item 2", unit_price=2.6, units=2, total_price=5.2
            ),
        ],
        total_price=15.7,
    )
